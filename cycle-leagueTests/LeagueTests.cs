﻿using CycleLeagueApp.cycleLeague;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace CycleLeagueApp.cycleLeague.Tests
{
    [TestClass()]
    public class LeagueTests
    {
        private CycleLeague league;
        private int numberOfCompetitors;

        [TestInitialize]
        public void setup()
        {
            //arrange
            league = new CycleLeague("Carnegie", 5);
            numberOfCompetitors = 10;
        }

        [TestMethod()]
        public void RunLeague_HappyPath()
        {
            //arrange
            int numberOfEvents = 5;

            //act
            CycleLeague result = League.RunLeague(numberOfEvents, numberOfCompetitors, league);

            //assert
            List<Competitor> competitors = result.GetCompetitors().Cast<Competitor>().ToList();
            Assert.AreEqual(competitors.Count, league.GetCompetitors().Count); //Assert that everyone in the league competed
            Assert.IsTrue(competitors.All(competitor => competitor.getEventsCompeted() == 5)); //Assert that everyone competed in 5 events
            Assert.IsTrue(competitors.Any(competitor => competitor.getAwards().Count == 3)); //Assert that someone won
            Assert.IsTrue(competitors.Any(competitor => competitor.getAwards().Count == 0)); //Assert that someone lost
            Assert.IsTrue(competitors.Where(competitor => competitor.getAwards().Count == 0).Count() == 9); //Assert that 9 people lost
        }


        [TestMethod()]
        public void CreateCompetitors_CreatesTheRightNumberOfCompetitors()
        {
            //act
            List<Competitor> result = League.CreateCompetitors(league, numberOfCompetitors);

            //assert
            Assert.IsTrue(result.Count() == numberOfCompetitors);
            result.ForEach(r => Assert.IsTrue(r is Competitor));
        }

        [TestMethod()]
        public void AssignRandomPoints_AssignsValuesGreaterThanZero()
        {
            //arrange
            List<Competitor> competitors = League.CreateCompetitors(league, numberOfCompetitors);

            //act
            League.AssignRandomPoints(competitors);

            //assert
            competitors.ForEach(c => Assert.IsNotNull(c.getPoints()));
            competitors.ForEach(c => Assert.IsTrue(c.getPoints() > 0));
        }

        [TestMethod()]
        public void AwardPrize_AwardsPrizesToWinners()
        {
            //arrange
            CycleLeague newLeague = new CycleLeague("New League", 1);
            Competitor thalita = new Competitor(newLeague, "Thalita", "Leeds Beckett", "Adult");
            Competitor duncan = new Competitor(newLeague, "Duncan", "Leeds Beckett", "Adult");
            duncan.setResult(10); //Duncan got 10 points
            thalita.setResult(20); //Thalita got 20 points

            //act
            League.AwardPrize(newLeague);

            //assert
            List<Award> duncanAwards = duncan.getAwards().Cast<Award>().ToList();
            List<Award> thalitaAwards = thalita.getAwards().Cast<Award>().ToList();
            Assert.AreEqual(thalitaAwards.Count, 3); //Thalita got 3 prizes
            Assert.IsTrue(condition: thalitaAwards.Any(award => "League Champion".Equals(award.getName()))); //Thalita got the League Champion prize
            Assert.IsTrue(condition: thalitaAwards.Any(award => "Hilly 19 Trophy".Equals(award.getName()))); //Thalita got the Hilly 19 prize
            Assert.IsTrue(condition: thalitaAwards.Any(award => "Triangle Trophy".Equals(award.getName()))); //Thalita got the Triangle prize
            Assert.AreEqual(duncanAwards.Count, 0); //Duncan didn't get any prizes ;)
        }

        [TestCleanup]
        public void cleanup()
        {
            //arrange
            league = null;
            numberOfCompetitors = 0;
        }
    }
}